# Pirates of the Caribbean Resources.
This repository contains all the resource phase files for Pirates of the Caribbean.

## Setup
* After cloning, check `WeaponGlobals.pkl`, `ItemGlobals.pkl` and `DropGlobals.pkl` in phase_2/etc to make sure they have LF line endings.